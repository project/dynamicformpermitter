<?php

namespace Drupal\dynamicformpermitter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormValidator;
use Drupal\Core\Form\FormValidatorInterface;
use Drupal\Core\Form\OptGroup;

/**
 * Provides validation of form submissions but allows AJAX-populated selects.
 *
 * This replaces Drupal\Core\Form\FormValidator by way of our overriding the
 * form_validator service.  See src/DynamicformpermitterServiceProvider.php.
 */
class DynamicFormValidator extends FormValidator implements FormValidatorInterface {
  /**
   * Performs validation of elements that are not subject to limited validation.
   *
   * @param array $elements
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form. The current user-submitted data is stored
   *   in $form_state->getValues(), though form validation functions are passed
   *   an explicit copy of the values for the sake of simplicity. Validation
   *   handlers can also $form_state to pass information on to submit handlers.
   *   For example:
   *     $form_state->set('data_for_submission', $data);
   *   This technique is useful when validation requires file parsing,
   *   web service requests, or other expensive requests that should
   *   not be repeated in the submission step.
   */
  protected function performRequiredValidation(&$elements, FormStateInterface &$form_state) {
    // There's probably a way to add the module_handler service to this class
    // we're overriding, but i don't know it, so let the dependency injection
    // gods have mercy on my soul.
    $fields = \Drupal::moduleHandler()->invokeAll('dynamicformpermitter_allow');

    // Here's the kind of information "$elements" (which is really one element
    // at a time, or at any rate one field at a time, not all the elements on
    // the form):
    //   - #type: "checkboxes"
    //   - #element_validate: [
    //       "Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsButtonsWidget",
    //       "validateElement"
    //     ]
    //  and #default_value, #options
    //   - #field_name
    //   - #parents (same as field name)

    // If it's any other field, do exactly the normal validation.
    if (!isset($elements['#field_name']) || !in_array($elements['#field_name'], $fields)) {
      return parent::performRequiredValidation($elements, $form_state);
    }
    // Otherwise, still hew as closely as possible to the old approach.

    // @TODO ways to do this way better:
    // inspect element_validate for … specially named? … validators and cede to
    // them, that way we can at least be sure there's some effort to validate.
    // OR!  We have a hook that returns the additional options that should be
    // allowed, and just stick them in, and then run the parent validation
    // function, so that we don't munch its code *at all*.  Huh, i wonder if
    // there's a pre-validation hook or somesuch we could do that in without
    // having to override the form_validator service.

    // Verify that the value is not longer than #maxlength.
    if (isset($elements['#maxlength']) && mb_strlen($elements['#value']) > $elements['#maxlength']) {
      $form_state->setError($elements, $this->t('@name cannot be longer than %max characters but is currently %length characters long.', ['@name' => empty($elements['#title']) ? $elements['#parents'][0] : $elements['#title'], '%max' => $elements['#maxlength'], '%length' => mb_strlen($elements['#value'])]));
    }

    if (isset($elements['#options']) && isset($elements['#value'])) {
      if ($elements['#type'] == 'select') {
        $options = OptGroup::flattenOptions($elements['#options']);
      }
      else {
        $options = $elements['#options'];
      }
      if (is_array($elements['#value'])) {
        $value = in_array($elements['#type'], ['checkboxes', 'tableselect']) ? array_keys($elements['#value']) : $elements['#value'];
        foreach ($value as $v) {
          if (!isset($options[$v])) {
            // $form_state->setError($elements, $this->t('An illegal choice has been detected. Please contact the site administrator.'));
            // $this->logger->error('Illegal choice %choice in %name element.', ['%choice' => $v, '%name' => empty($elements['#title']) ? $elements['#parents'][0] : $elements['#title']]);
            $this->logger->error('Allowed unvalidated %choice in %name element.', ['%choice' => $v, '%name' => empty($elements['#title']) ? $elements['#parents'][0] : $elements['#title']]);
          }
        }
      }
      // Non-multiple select fields always have a value in HTML. If the user
      // does not change the form, it will be the value of the first option.
      // Because of this, form validation for the field will almost always
      // pass, even if the user did not select anything. To work around this
      // browser behavior, required select fields without a #default_value
      // get an additional, first empty option. In case the submitted value
      // is identical to the empty option's value, we reset the element's
      // value to NULL to trigger the regular #required handling below.
      // @see \Drupal\Core\Render\Element\Select::processSelect()
      elseif ($elements['#type'] == 'select' && !$elements['#multiple'] && $elements['#required'] && !isset($elements['#default_value']) && $elements['#value'] === $elements['#empty_value']) {
        $elements['#value'] = NULL;
        $form_state->setValueForElement($elements, NULL);
      }
      elseif (!isset($options[$elements['#value']])) {
        $form_state->setError($elements, $this->t('An illegal choice has been detected. Please contact the site administrator.'));
        $this->logger->error('Illegal choice %choice in %name element.', ['%choice' => $elements['#value'], '%name' => empty($elements['#title']) ? $elements['#parents'][0] : $elements['#title']]);
      }
    }
  }
}
