<?php

namespace Drupal\dynamicformpermitter;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Drupal\Core\DependencyInjection\ServiceProviderInterface;

/**
 * Permit dynamic forms to add extra options to select fields etc.
 *
 * Modules that make use of this must do their own validation.
 */
class DynamicformpermitterServiceProvider extends ServiceProviderBase implements ServiceProviderInterface {
  public function alter(ContainerBuilder $container) {
    $definition = $container->getDefinition('form_validator');
    $definition->setClass('Drupal\dynamicformpermitter\DynamicFormValidator');
  }
}
